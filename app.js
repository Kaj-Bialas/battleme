var express = require('express'),
	app = express(),
	server = require('http').createServer(app),
	io = require('socket.io').listen(server),
	users = {},
    path = require('path'),
    //Ograniczenie uzytkownikow, tylko na potrzeby dema -- TODO: Usunąć po wprowadzeniu priv sesji
    usersCount = 0;

//Port serwera
server.listen(3000);

app.get('/', function(req, res){
	res.sendfile(__dirname + '/index.html');
});

//Routing źródeł --TODO:Doczytać
app.use(express.static(
    path.join(__dirname, 'public')
));

io.sockets.on('connection', function(socket){

        socket.on('new user', function(data, callback){
            if (usersCount < 2) {
                if (data in users) {
                    callback(false);
                } else {
                    usersCount++;
                    console.log('Obecni userzy: ' + usersCount);
                    callback(true);
                    socket.nickname = data;
                    users[socket.nickname] = socket;
                    updateNicknames();
                }
            } else {
                    console.log("Mamy już 2 mld graczy ONLINE - Dedyk zaraz umrze, nie WOLNO wpuścić następnego gracza ;)");
                    callback('false2');
                }
        });


	
	function updateNicknames(){
		io.sockets.emit('usernames', Object.keys(users));
	}


    //Socket do obsługi chatu - Priv na później... TODO:
	socket.on('send message', function(data, callback){
		var msg = data.trim();
		console.log('User: ' + socket.nickname+ ' id: ' + socket.id + ' send message: ' + msg);
		if(msg.substr(0,3) === '/w '){
			msg = msg.substr(3);
			var ind = msg.indexOf(' ');
			if(ind !== -1){
				var name = msg.substring(0, ind);
				var msg = msg.substring(ind + 1);
				if(name in users){
					users[name].emit('whisper', {msg: msg, nick: socket.nickname});
					console.log('message sent is: ' + msg);
					console.log('Whisper!');
				} else{
					callback('Error!  Enter a valid user.');
				}
			} else{
				callback('Error!  Please enter a message for your whisper.');
			}
		} else{
			io.sockets.emit('new message', {msg: msg, nick: socket.nickname});
		}
	});


    //Obsługa gry
    socket.on('game', function(data, callback){
        var msg = data;
        console.log('User: ' + socket.nickname+ ' id: ' + socket.id + ' wybrał pola: ' + msg);
        io.sockets.emit('opponent ships', {msg: msg, nick: socket.nickname});
    });
    socket.on('new shot', function(data, callback){
        var msg = data;
        console.log('User: ' + socket.nickname+ ' id: ' + socket.id + ' strzelił pole: ' + msg);

        io.sockets.emit('new opponent shot', {msg: msg, nick: socket.nickname});
    });




	socket.on('disconnect', function(data){
		if(!socket.nickname) return;
        //Ograniczenie uzytkownikow, tylko na potrzeby dema -- TODO: Usunąć po wprowadzeniu priv sesji
        usersCount--;
		delete users[socket.nickname];
		updateNicknames();
	});
});